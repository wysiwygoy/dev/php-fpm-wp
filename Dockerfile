#
# PHP-FPM container for running WordPress
# Based on https://hub.docker.com/r/zaherg/php-7.0-xdebug-alpine/~/dockerfile/
#

FROM php:7.0-fpm-alpine
MAINTAINER Jarno Antikainen <jarno.antikainen@wysiwyg.fi>

LABEL Description="This image is used for running WordPress on PHP-FPM." Vendor="Wysiwyg Oy"

RUN apk update \
    && apk add  --no-cache g++ make autoconf \
    && docker-php-source extract \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && docker-php-source delete \
    && docker-php-ext-install mysqli \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_port=9000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_connect_back=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && rm -rf /tmp/*

